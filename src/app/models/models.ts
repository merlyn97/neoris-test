export interface TableModel {
    titles: Array<string>,
    data: Array<dataPokemon>
}

export interface dataPokemon {
    name: string,
    image: string,
    attack: number,
    defending: number,
    icon: Array<IconData>,
    id: number
}

export interface IconData{
    uri: string,
    key: string
}

export interface IconDataEmitter{
    id: number
}