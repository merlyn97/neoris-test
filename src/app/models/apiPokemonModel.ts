export interface PokemonApi {
    attack: number;
    defense: number
    hp: number,
    id: number,
    id_author: number,
    image: string
    name: string,
    type: string,
    typeForm: 'EDIT' | 'NEW' | ''
}

export interface PokemonApiCreate {
    id?: number,
    name: string,
    image: string,
    attack: number,
    defense: number,
    hp?: number,
    type: string,
    idAuthor?: number
}