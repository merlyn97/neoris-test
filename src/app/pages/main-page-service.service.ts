import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { PokemonApi, PokemonApiCreate } from '../models/apiPokemonModel';

@Injectable({
  providedIn: 'root'
})
export class MainPageServiceService {

  urlBase: string = "https://bp-pokemons.herokuapp.com/";
  constructor(private http: HttpClient) { }

  getPokemonsByAuthorID(id: number): Observable<Array<PokemonApi>>{

    return this.http.get<Array<PokemonApi>>(`${this.urlBase}2?idAuthor=${id}`);

  }


  deletePokemonById(id: number) {
    return this.http.delete<any>(`${this.urlBase}${id}`);
  }

  getPokemonById(id: number) {
    return this.http.get<PokemonApi>(`${this.urlBase}${id}`);
  }

  postPokemon(pokemonObj: PokemonApiCreate) {
    return this.http.post<PokemonApi>(`${this.urlBase}`, pokemonObj);
  }

  putPokemon(pokemonObj: PokemonApiCreate, id: number) {
    return this.http.put<PokemonApi>(`${this.urlBase}${id}`, pokemonObj);
  }

}
