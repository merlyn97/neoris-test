import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page.component';
import { MainPageRoutingModule } from './main-page-routing.module';
import { PokemonTableModule } from '../../components/pokemon-table/pokemon-table.module';
import { FormModule } from 'src/app/components/form/form.module';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    MainPageComponent
  ],
  imports: [
    CommonModule,
    MainPageRoutingModule,
    PokemonTableModule,
    FormModule,
    FormsModule
  ]
})
export class MainPageModule { }
