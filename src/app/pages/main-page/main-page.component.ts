import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MainPageServiceService } from '../main-page-service.service';
import { IconDataEmitter, TableModel } from 'src/app/models/models';
import { PokemonApi } from 'src/app/models/apiPokemonModel';
import { Observable } from 'rxjs';
import { PokemonTableComponent } from 'src/app/components/pokemon-table/pokemon-table.component';
import { FormComponent } from 'src/app/components/form/form.component';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  showFormPipe: boolean = false;

  @ViewChild('tableElementRef') elementRef: PokemonTableComponent;
  @ViewChild('formElementRef') elementForm: FormComponent;
  pokemonToEdit: PokemonApi;
  idPokemon: number = 0;

  constructor(private mainPageService: MainPageServiceService) { }

  sus:  Observable<Array<PokemonApi>> = this.mainPageService.getPokemonsByAuthorID(1);

  ngOnInit(): void {
  }

  showForm(){

    if(this.elementForm != undefined)
      this.elementForm.clearForm();

      this.pokemonToEdit = {
        name: '',
        attack: 0,
        defense: 0,
        hp: 0,
        id: 0,
        id_author: 0,
        image: '',
        type: '',
        typeForm: 'NEW'
      };

    this.showFormPipe = true;
  }

  closeForm(e: boolean){
    this.showFormPipe = e;
    this.elementRef.buildTableData();
  }

  editEmmit(e: IconDataEmitter){
    
    this.mainPageService.getPokemonById(e.id).subscribe(resp => {

      console.log(e.id);
      
      console.log(resp);
      
      this.pokemonToEdit = resp;
      this.pokemonToEdit.typeForm = 'EDIT';
      this.showFormPipe = true;
    });
    
  }

  deleteEmmit(e: IconDataEmitter){
    
    this.mainPageService.deletePokemonById(e.id).subscribe(() => {});

    this.elementRef.buildTableData();
    this.showFormPipe = false;
    
  }

  onEnter(){
    console.log(this.idPokemon);
    this.elementRef.findPokemonByID();
  }

  printList(){
    this.elementRef.buildTableData();
  }
}
