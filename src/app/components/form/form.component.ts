import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PokemonApi, PokemonApiCreate } from 'src/app/models/apiPokemonModel';
import { MainPageServiceService } from 'src/app/pages/main-page-service.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  sliderPoints:Array<number> = [];
  @Output() closeFormPipe = new EventEmitter<boolean>();

  @Input()
  pokemonToEdit: PokemonApi;
  
  pokemonCreate: PokemonApiCreate;
  
  totalWidthString: string = '0%';
  totalWidthNumber: number;

  constructor(private mainPageServiceService: MainPageServiceService) { }

  ngOnInit(): void {

    
    this.initSlider();

  }
  initSlider() {
    
    let con = 5;
    while(con < 101 ){
      this.sliderPoints.push(con);
      con = con + 5;
    }

  }

  getPointSelected(pointSelected: number, sliderNumber: number){

    console.log("hola mundo" + pointSelected);
    
    
    if(sliderNumber == 1){
      this.pokemonCreate.attack = pointSelected;
      this.totalWidthString = pointSelected + '%';
    } else {
      this.pokemonCreate.defense = pointSelected;
    }
    
  }

  closeForm(){
    this.clearForm();

    this.closeFormPipe.emit(false);
  }
  clearForm() {
    this.pokemonToEdit.name = '';
    this.pokemonToEdit.image = '';
    this.pokemonToEdit.attack = 0;
    this.pokemonToEdit.defense = 0;
  }

  savePokemon() {    
    switch(this.pokemonToEdit.typeForm){

      case 'EDIT':
        this.saveEditPokemon();
      break;

      case 'NEW':

        this.saveNewPokemon();
        break;
    }
  }
  saveNewPokemon() {

    console.log(this.pokemonToEdit.attack);
    

    
    this.pokemonCreate = {
      name: this.pokemonToEdit.name,
      attack: this.pokemonToEdit.attack,
      image: this.pokemonToEdit.image,
      defense: this.pokemonToEdit.defense,
      hp: 2,
      type: 'raro',
      idAuthor: 2
    };

    this.mainPageServiceService.postPokemon(this.pokemonCreate).subscribe(response => {
      this.closeFormPipe.emit(false);
    });
    

    
  }
  saveEditPokemon() {

    this.pokemonCreate = {
      name: this.pokemonToEdit.name,
      attack: this.pokemonToEdit.attack,
      image: this.pokemonToEdit.image,
      defense: this.pokemonToEdit.defense,
      hp: 2,
      type: 'raro',
      idAuthor: 2,
      id: this.pokemonToEdit.id
    };

    this.mainPageServiceService.putPokemon(this.pokemonCreate, this.pokemonToEdit.id).subscribe(response => {
      this.closeFormPipe.emit(false);
    });
  }

  attackSlider(value: number) {
    if (value >= 100) {
      return Math.round(value / 100);
    }
    

    return value;
  }

  sliderDefense(value: number) {
    if (value >= 100) {
      return Math.round(value / 100);
    }

    return value;
  }

}
