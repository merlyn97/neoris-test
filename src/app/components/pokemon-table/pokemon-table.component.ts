import { Component, Input, OnInit, EventEmitter, Output, NgZone, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { IconDataEmitter, TableModel } from 'src/app/models/models';
import { MainPageServiceService } from 'src/app/pages/main-page-service.service';

@Component({
  selector: 'app-pokemon-table',
  templateUrl: './pokemon-table.component.html',
  styleUrls: ['./pokemon-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class PokemonTableComponent implements OnInit {

  
  tableData: TableModel = {
    titles: ['Nombre', 'Imagen', 'Ataque', 'Defensa', 'Acciones'],
    data: []
  }

  totalRows: number;
  totalRowsArray: Array<number> = [];
  
  totalTd: number;
  totalTdArray: Array<number> = [];

  @Output() editEmmitPipe = new EventEmitter<IconDataEmitter>();
  @Output() deleteEmmitPipe = new EventEmitter<IconDataEmitter>();

  @Input()
  idPokemon: number;

  constructor(private mainPageService: MainPageServiceService, private zone:NgZone, private cdRef: ChangeDetectorRef) { }


  ngOnInit(): void {

    this.buildTableData();
  }
  buildTableData() {
    this.mainPageService.getPokemonsByAuthorID(2).subscribe(response => {
    
      this.tableData.data = response.map(pokemonItem => {    
        return {
          name: pokemonItem.name,
          image: pokemonItem.image,
          attack: pokemonItem.attack,
          defending: pokemonItem.defense,
          icon: [{uri:'../../../assets/icons/edit.svg', key: 'edit'},{uri:'../../../assets/icons/trash.svg', key:'trash'}],
          id: pokemonItem.id
        }
      });  
    
      this.cdRef.detectChanges();
  });

  }

  editEmmit(id: number) {
    this.editEmmitPipe.emit({id});
  }

  deleteEmmit(id: number) {
    this.deleteEmmitPipe.emit({id});
  }

  findPokemonByID(){
    
    if(this.idPokemon != 0)
    this.mainPageService.getPokemonById(this.idPokemon).subscribe(result => {
      
      if(result !== null)
        this.tableData.data = [
        {
          name: result.name,
          image: result.image,
          attack: result.attack,
          defending: result.defense,
          icon: [{uri:'../../../assets/icons/edit.svg', key: 'edit'},{uri:'../../../assets/icons/trash.svg', key:'trash'}],
          id: result.id
        }
        ];
      else 
        this.buildTableData();
    });
  }

}
