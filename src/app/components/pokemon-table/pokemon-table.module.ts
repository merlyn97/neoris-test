import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PokemonTableComponent } from './pokemon-table.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PokemonTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule

    
  ],
  exports: [PokemonTableComponent]
})
export class PokemonTableModule { }
